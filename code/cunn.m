function c=cunn(dp,t,press)

% calculates cunningham's correction factor
% out: 
%  cunningham correction factor [#]
% in:
%  particle diameter [m]
%  temperature [K]
%  pressure [Pa]

c=1.0+ rlambda(t,press) ./dp .*(2.514+0.800*exp(-0.55.*dp./rlambda(t,press)));


