% This function calculates viscosity of air at given temperature (in air)
% according to Schlichting (1979).
% 
%   (equations from Baron and Willeke, 2nd ed. pp. 66)
%
% IN:
% T = temperature [K]
%   applicaple to range 180-2000 K, viscosity independent of pressure !
%
% OUT:
% vv = viscosity of air [kg cm^-1 s^-1 = Pa*s]
%
% References
%   Baron and Willeke pp. 66
%   Rader, D.J. (1990) Momentum slip correction factor for small particles
%       in nine common gases. J. Aerosol Sci 21:161-168.
%   Schlicting, H. (1979) Boundary-Layer Theory. New York: McGraw Hill.

function [nyy]=visc2(temp)

% viscosity as a function of temperature

% for air:
nyy_ref=18.203e-6;

% Sutherland interpolation constant [K]
S=110.4;

% reference temperature = 293.15 K
temp_ref=293.15;

nyy=nyy_ref.*((temp_ref+S)./(temp+S)).*((temp./temp_ref).^(3/2));
% old: visc.m
%vv=(174.+0.433*(temp-273.))*1.0e-7;

