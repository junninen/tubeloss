% This function calculates mean free path of "air molecule"
%
% In:
%  temperature [K]
%  pressure [Pa]

% Out:
%  mean free path of air molecule [m]
function r=rlambda(t,press)

% dm=average diameter of "air molecule"
dm=3.7e-10;
% avogadros number
avoc=6.022e23;
k=8.3143;

r=k*t/(sqrt(2.)*avoc*press*pi*dm*dm);
