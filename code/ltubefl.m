% This function calculates losses due to diffusion in circular pipes.
% Original by Pasi Aalto.
% Modified by Tuukka Petaja 2002
% Modified by Heikki Junninen 2013
%
% IN:
%   dpp [m] = particle diameter
%   plength [m] = pipe diameter
%   pflow [m3/s] = flow through pipe
%   temp [K] = temperature
%   press [Pa] = pressure
%
% OUT:
%   transport efficiency (0-1)
%
function res=ltubefl(dpp,plength,pflow,temp,press)


rmuu=pi*diffuus(dpp,temp,press)*plength./pflow;

I1=rmuu<0.02;
I2=~I1;

res1=1-2.56*rmuu(I1).^(2/3)+1.2*rmuu(I1)+0.177*rmuu(I1).^(4/3);
res2=0.819*exp(-3.657*rmuu(I2))+0.097*exp(-22.3*rmuu(I2))+0.032*exp(-57*rmuu(I2));

if isempty(res1)
    res=res2;
elseif isempty(res2)
    res=res1;
else
    res=NaN(1,numel(rmuu));
    res(I1)=res1;
    res(I2)=res2;
end

% for i=1:length(dpp)
%     
%     if rmuu(i) < 0.02
%         
%         res(i)=1-2.56*rmuu(i).^(2/3)+1.2*rmuu(i)+0.177*rmuu(i).^(4/3);
%         
%     else
%         
%         res(i)=0.819*exp(-3.657*rmuu(i))+0.097*exp(-22.3*rmuu(i))+0.032*exp(-57*rmuu(i));
%         
%     end
%     
% end

