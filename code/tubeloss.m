% This function calculates losses in circular tubes.
%
%
% IN:
%   Dp [m] = particle diameter
%   pipeflow [m3/s] = volumetric flow rate
%   pipediameter [m] = diameter of pipe
%   t [K] = temperature
%   press [Pa] = pressure
%
% OUT:
%
% ASSUMPTIONS:
%   particles have unit density (1000 kg/m3)
%
% Reference:
%   equations from:
%       Brockmann, J.E. (2001), Sampling and Transport of Aerosols (Ch 8)
%           In Baron, P.A. and Willeke, K. (2001)
%           Aerosol Measurement, Principles, Techniques, and Applications,
%           2nd edition, John Wiley & Sons Inc. New York 1131pp.
%   diffusion losses (analytical equation, laminar flow)
%       Gormley and Kennedy (1949) Diffusion from a stream flowing through
%       a cylindrical tube. Proc. R. Irish Acad. 52A: pp.163-169.


function [totalloss,Re]=tubeloss(Dp,pipeflow,pipediameter,pipelength,t,press,Re_lim)

if nargin==6
    Re_lim=4000;
end
roop=1000;
pipearea=pi/4*pipediameter^2;

% characteristic velocity in tube:
velocity=pipeflow/pipearea;

% air density:
density=1.29*(273/t)*(press/101325);

% particle relaxation time:
tau=(roop.*(Dp.^2).*cunn(Dp,t,press))./(18.*visc2(t));


% calculate Reynolds number in tube:
Re=reynolds(pipeflow,pipediameter,t,press);

% calculate Schmidt number:
Sc=visc2(t)./(density.*diffuus(Dp,t,press));
%Sc=repmat(Sc,1,numel(Re));

% calculate Sherwood number:
Sh=0.0118*Re.^(7/8).*(Sc.^(1/3));

% calculate particle Stokes number
Stk=(tau.*velocity)./(pipediameter);

% dimensionless parameter ksii=pi*diffusivity*lenght/volumetric flow rate
ksii=(pi.*diffuus(Dp,t,press).*pipelength)./(pipeflow);

txt=sprintf('Reynolds number = %s',num2str(Re));
%disp(txt)

% diffusion loss for laminar flow:
% Gormley and Kennedy (1949)
difflam=ltubefl(Dp,pipelength,pipeflow,t,press);

% general equation (Baron and Willeke pp. 174)
diffturb=exp(-ksii.*Sh);


% turbulent inertial deposition in sampling lines:
% Baron and Willeke pp 177

% dimensionless relaxation time
taud=0.0395.*Stk.*(Re.^(3/4));

% dimensionless deposition velocity
velocityd=6e-4.*(taud.^2)+2e-8.*Re;

% deposition velocity for turbulent inertial deposition:
velocity_inertturb=(velocity.*velocityd)./(5.03.*Re.^(1/8));

% transport efficiency for turbulent inertial deposition
inertturb=exp((-pi*pipediameter*pipelength.*velocity_inertturb)./pipeflow);

% 90 degree bend (hat + inlet)
% Baron and Willeke pp 180 (Pui et al 1987)
%inertturbbend=exp(-2.823.*Stk.*360/pi);
inertturbbend=exp(-2.823.*Stk.*pi/1.8);

% total loss to walls:
I1=Re<Re_lim;
I2=Re>=Re_lim;

totalloss=NaN(size(Re));
totalloss(I1)=difflam(I1);
totalloss(I2)=diffturb(I2).*inertturb(I2);
%totalloss(I2)=diffturb(I2).*inertturb(I2).*inertturbbend(I2);
    
% if Re<2000
%     totalloss=difflam;
% else
%     totalloss=diffturb.*inertturb.*inertturbbend;
% end

% figure
% semilogx(Dp,difflam,Dp,diffturb,Dp,inertturb,Dp,inertturbbend,Dp,totalloss)
% %hold on
% legend('laminar flow diffusion loss','turbulent flow diffusion loss','turbulent inertial loss','turbulent inertial loss (90deg bend)','total loss',0)
% grid on
% xlabel('Particle diameter [Dp]')
% ylabel('transport efficiency [0-1]')

