# README #

### What is this repository for? ###

This is a toolbox for estimating tubelosses in circular tube


 Reference:
   equations from:
       Brockmann, J.E. (2001), Sampling and Transport of Aerosols (Ch 8)
           In Baron, P.A. and Willeke, K. (2001)
           Aerosol Measurement, Principles, Techniques, and Applications,
           2nd edition, John Wiley & Sons Inc. New York 1131pp.
   diffusion losses (analytical equation, laminar flow)
       Gormley and Kennedy (1949) Diffusion from a stream flowing through
       a cylindrical tube. Proc. R. Irish Acad. 52A: pp.163-169.


### How do I get set up? ###

Download the reposisitory to an empty folder, add this folder to matlab path
for GUI run:
>> H_gui_tubelosses_1tube

for command line:
>> help tubelosses



### Who do I talk to? ###

* Repo owner or admin
Heikki Junninen, heikki.junninen@gmail.com

* Other community or team contact
Pasi Aalto
Tuukka Petäjä