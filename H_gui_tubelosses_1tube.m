function varargout = H_gui_tubelosses_1tube(varargin)
% H_GUI_TUBELOSSES_1TUBE MATLAB code for H_gui_tubelosses_1tube.fig
%      H_GUI_TUBELOSSES_1TUBE, by itself, creates a new H_GUI_TUBELOSSES_1TUBE or raises the existing
%      singleton*.
%
%      H = H_GUI_TUBELOSSES_1TUBE returns the handle to a new H_GUI_TUBELOSSES_1TUBE or the handle to
%      the existing singleton*.
%
%      H_GUI_TUBELOSSES_1TUBE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in H_GUI_TUBELOSSES_1TUBE.M with the given input arguments.
%
%      H_GUI_TUBELOSSES_1TUBE('Property','Value',...) creates a new H_GUI_TUBELOSSES_1TUBE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before H_gui_tubelosses_1tube_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to H_gui_tubelosses_1tube_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help H_gui_tubelosses_1tube

% Last Modified by GUIDE v2.5 01-Jun-2013 17:42:44

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @H_gui_tubelosses_1tube_OpeningFcn, ...
    'gui_OutputFcn',  @H_gui_tubelosses_1tube_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before H_gui_tubelosses_1tube is made visible.
function H_gui_tubelosses_1tube_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to H_gui_tubelosses_1tube (see VARARGIN)

% Choose default command line output for H_gui_tubelosses_1tube
handles.output = hObject;

% defaults
L=0.4; %m
T=20+273; %K
P=101325; %Pa
Dp=1; %m

L=1; %m
ID=8;%mm
F=10; %lpm

datE=get(handles.env_cond,'data');

datE{1,1}=T;
datE{1,2}=' K';
datE{2,1}=P;
datE{2,2}=' Pa';
datE{3,1}=Dp;
datE{3,2}=' nm';

set(handles.env_cond,'data',datE);

datF=get(handles.flow_cond,'data');

datF{1,1}=ID;
datF{1,2}=' mm';
datF{2,1}=L;
datF{2,2}=' m';
datF{3,1}=F;
datF{3,2}=' l/min';

set(handles.flow_cond,'data',datF);

% Update handles structure
guidata(hObject, handles);

calc_losses(handles)
make_plot_nice(handles.axes1)
% UIWAIT makes H_gui_tubelosses_1tube wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = H_gui_tubelosses_1tube_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function Re_Callback(hObject, eventdata, handles)
% hObject    handle to Re (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Re as text
%        str2double(get(hObject,'String')) returns contents of Re as a double


% --- Executes during object creation, after setting all properties.
function Re_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Re (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tr_Callback(hObject, eventdata, handles)
% hObject    handle to tr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tr as text
%        str2double(get(hObject,'String')) returns contents of tr as a double


% --- Executes during object creation, after setting all properties.
function tr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function loss_Callback(hObject, eventdata, handles)
% hObject    handle to loss (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of loss as text
%        str2double(get(hObject,'String')) returns contents of loss as a double


% --- Executes during object creation, after setting all properties.
function loss_CreateFcn(hObject, eventdata, handles)
% hObject    handle to loss (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when entered data in editable cell(s) in env_cond.
function env_cond_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to env_cond (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)

calc_losses(handles)


% --- Executes when entered data in editable cell(s) in flow_cond.
function flow_cond_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to flow_cond (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)

calc_losses(handles)

function calc_losses(handles)

datE=get(handles.env_cond,'data');
datF=get(handles.flow_cond,'data');

r=datF{1,1}/2;%mm
r=r*1e-3;%m
L=datF{2,1};%m

%area
A=pi*r^2; %m2
V=A*L; %m3

%flow
F=datF{3,1}; %lpm
F=F*1e-3; %m3/min
F=F/60; %m3/s

% gas velocity
v=F/A; %m/s

%residence time
tm=L/v;

Dp=datE{3,1}*1e-9; %m
P=datE{2,1};
T=datE{1,1};

%[totalloss,Re]=tubeloss(Dp,pipeflow,pipediameter,pipelength,t,press)
[tran,Re]=tubeloss(Dp,F,r*2,L,T,P);
loss=1-tran;

datR{1,1}=sprintf('%1.1e',A);
datR{1,2}=' m2';
datR{2,1}=sprintf('%1.1e',V);
datR{2,2}=' m3';
datR{3,1}=sprintf('%1.2f',v);
datR{3,2}=' m/s';
datR{4,1}=sprintf('%1.2e',tm);
datR{4,2}=' s';

set(handles.char,'data',datR);
set(handles.Re,'String',sprintf('%4.0f',Re))
set(handles.loss,'String',sprintf('%2.1f%%',loss*100))
set(handles.tr,'String',sprintf('%2.1f%%',tran*100))

if Re<=2000
    set(handles.flow_regime,'String','LAMINAR','fontsize',16,'foregroundcolor',[.1 .1 .1],'FontName'   , 'AvantGarde')
elseif Re<=4000
    set(handles.flow_regime,'String','TRANSITIONAL','fontsize',16,'foregroundcolor',[.1 .1 .1],'FontName'   , 'AvantGarde')
else
    set(handles.flow_regime,'String','TURBULENT','fontsize',16,'foregroundcolor',[.1 .1 .1],'FontName'   , 'AvantGarde')
end
%vary flows
F1=F-F*0.9;
F2=F+F*0.9;
Fs=F1:(F2-F1)/19:F2;
Fs=sort([Fs,F]);
[transms,Re]=tubeloss(Dp,Fs,r*2,L,T,P,4000);


lh=findobj(handles.axes1,'Tag','flin');if ~isempty(lh),delete(lh),end
lh=findobj(handles.axes1,'Tag','flin_trans');if ~isempty(lh),delete(lh),end
lh=findobj(handles.axes1,'Tag','fone');if ~isempty(lh),delete(lh),end
lh=findobj(handles.axes1,'Tag','fdot');if ~isempty(lh),delete(lh),end

I1=Re<2000;

lh=line(Fs*1e3*60,transms*100,'parent',handles.axes1,'tag','flin');
lh2=line([F F]*1e3*60,[0,tran*100],'color',[0.1 0.1 0.1],'tag','fone');
lh3=line([F]*1e3*60,[tran*100],'color',[0.1 0.1 0.1],'marker','o','tag','fdot');
lh4=line([F1,F]*1e3*60,[tran tran]*100,'color',[0.1 0.1 0.1],'tag','fdot');

I1=Re<2000;

lh=line(Fs*1e3*60,transms*100,'parent',handles.axes1,'tag','flin');
lh2=line([F F]*1e3*60,[0,tran*100],'color',[0.1 0.1 0.1],'tag','fone');
lh3=line([F]*1e3*60,[tran*100],'color',[0.1 0.1 0.1],'marker','o','tag','fdot');
lh4=line([F1,F]*1e3*60,[tran tran]*100,'color',[0.1 0.1 0.1],'tag','fdot');


[transms,Re]=tubeloss(Dp,Fs,r*2,L,T,P,2000);
lh=line(Fs*1e3*60,transms*100,'parent',handles.axes1,'tag','flin_trans');


set(handles.axes1,'ylim',[0 100])
ylabel(handles.axes1,'Transmission [%]','fontsize',14,'fontweight','bold')
xlabel(handles.axes1,'Flow [l/min]','fontsize',14,'fontweight','bold')
%H_make_plot_nice(handles.axes1)

function make_plot_nice(ax)
%
hTitle=get(ax,'title');
hXLabel=get(ax,'xlabel');
hYLabel=get(ax,'ylabel');
hText=findobj(ax,'type','text');
set( ax                       , ...
    'FontName'   , 'Helvetica' ,...
    'FontWeight' , 'Bold'     );

set([hTitle, hXLabel, hYLabel, hText], ...
    'FontName'   , 'AvantGarde',...
     'FontWeight' , 'Bold'     );


set([hXLabel, hYLabel, hText] ,...
    'FontSize'   , 14          );

set( hTitle                   ,...
    'FontSize'   , 12         ,...
    'FontWeight' , 'bold'      );


set(ax, ...
    'Box'         , 'off'     , ...
    'TickDir'     , 'out'     , ...
    'TickLength'  , [.01 .01] , ...
    'XMinorTick'  , 'on'      , ...
    'YMinorTick'  , 'on'      , ...
    'YGrid'       , 'on'      , ...
    'XColor'      , [.2 .2 .2], ...
    'YColor'      , [.2 .2 .2], ...
    'LineWidth'   , 1.        );

set(ax,'linewidth',1.5)


% --- Executes on button press in copy.
function copy_Callback(hObject, eventdata, handles)
% hObject    handle to copy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

datE=get(handles.env_cond,'data');
datF=get(handles.flow_cond,'data');
datR=get(handles.char,'data');

ID=datF{1,1};%mm
L=datF{2,1};%m

%area
A=datR{1,1}; %m2
V=datR{2,1}; %m3

%flow
F=datF{3,1}; %lpm

% gas velocity
v=datR{3,1}; %m/s

%residence time
tm=datR{4,1}; %s

Dp=datE{3,1}; %nm
P=datE{2,1};
T=datE{1,1};

Re=get(handles.Re,'String')
tr=get(handles.tr,'String')
los=get(handles.loss,'String')

txt=sprintf('Temperature=%2.2f K; Pressure=%2.1f Pa; Particle diameter=%1.1f nm;\nTube inner diameter=%2.0f mm; Tube length=%1.1f m; Flow=%1.1f lpm;\nTube cross section=%s m2; Tube volume=%s m3;\nGas velocity=%s m/s; Residence time=%s s;\nReynolds number=%s; Transmission=%s; Losses=%s',T,P,Dp,ID,L,F,A,V,v,tm,Re,tr,los)

clipboard('copy', txt)

disp(txt)
